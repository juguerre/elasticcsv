# CONTRIBUTING

## Formatting

- This project uses **[`black`](https://black.readthedocs.io/en/stable/index.html)**
  formatter with line length of **100**.
- [pyproject.toml](pyproject.toml) includes `tool.black` configuration
- [.pre-commit-config.yaml](./.pre-commit-config.yaml) includes
   [pre-commit](https://pre-commit.com/) configuration.
  - install pre-commit configuration in this project with: `pre-commit install`


>Take a look to **[`black`](https://black.readthedocs.io/en/stable/integrations/index.html)** integrations with your IDE


## Issues

Issues are wellcome as a way to expose defects or the need for new features. Issues are the common way
to start the process of contribution:

 1. Issue
 2. Create WIP Merge Request from issue
 3. Pull newly created branch
 4. do some changes
 5. Push
 6. MR Ready
 7. Approbal
 8. Merge

## Merge Requests

- Merge requests are wellcome and will be processed via approbals.
- Simple `master` feature branch model is how we process contributions.
- Merge method is configured as `Merge commit with semi-linear history` so working with `rebase` in
  **your own branch** is promoted and expected as part of a merge request process.

> Never do rebase on branches where other users are pushing  commits.
> See this [`git rebase`](https://www.atlassian.com/git/tutorials/rewriting-history/git-rebase) tutorial
> if you are having doubts
