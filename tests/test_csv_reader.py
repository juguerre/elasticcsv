import logging
from time import sleep
from typing import Generator, Tuple

from box import Box
from pytest_mock import MockerFixture
from testcontainers.compose import DockerCompose

import elasticcsv
from elasticcsv.elastic_csv import _csv_reader, load_csv, download_csv

CSV_TEST_FILE = "./tests/resources/SINGULARES_ENTORNO.csv"
CSV_TEST_FILE_OUTPUT = "./tests/resources/SINGULARES_ENTORNO_out.csv"

logger = logging.getLogger(__name__)


def test_load_csv(config: Box, mocker: MockerFixture):
    mocker.patch("elasticcsv.elastic_wrapper.Elasticsearch", autospec=True)
    mocker.patch("elasticcsv.elastic_wrapper.streaming_bulk", new=mock_streaming_bulk)
    load_csv(config=config, index="", csv_file_name=CSV_TEST_FILE, delimiter=";")
    elasticcsv.elastic_wrapper.Elasticsearch.assert_called()


def test_load_csv2(config: Box):
    with DockerCompose("./", compose_file_name="docker-compose.yaml", pull=False) as compose:
        host = compose.get_service_host("elastic", 9200)
        port = compose.get_service_port("elastic", 9200)
        compose.wait_for(f"http://{host}:{port}/")
        load_regs = load_csv(
            config=config,
            index="input-singulares_entorno",
            csv_file_name=CSV_TEST_FILE,
            delimiter=";",
        )
        logger.info("Waiting for docs to be indexed")
        sleep(20)
        down_regs: int = download_csv(
            config=config,
            index="input-singulares_entorno",
            csv_file_name=CSV_TEST_FILE_OUTPUT,
            delimiter=";",
            file_mode="w",
        )

        assert load_regs == down_regs


def test_cst_v_reader(mocker: MockerFixture):
    read_gen = _csv_reader(CSV_TEST_FILE, delimiter=";", count_lines=True)
    count = 0
    for d in read_gen:
        count += 1
    assert count > 0


def mock_streaming_bulk(**kwargs) -> Generator[Tuple[bool, dict], None, None]:
    data_iterator = kwargs.get("actions")
    for d in data_iterator:
        yield True, d
