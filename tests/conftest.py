import logging

import pytest
import yaml
from box import Box

disable_loggers = ["elasticsearch"]


@pytest.fixture(scope="session")
def config():
    with open("./tests/resources/connection.yaml") as conn_file:
        conn_d = yaml.load(conn_file, Loader=yaml.FullLoader)
        config = Box(conn_d, box_dots=True)
    return config


def pytest_configure():
    for logger_name in disable_loggers:
        logger = logging.getLogger(logger_name)
        logger.setLevel(logging.CRITICAL)
        logger.disabled = True
